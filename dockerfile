# Stage 1
FROM microsoft/dotnet:2.2-sdk AS builder
WORKDIR /source

# caches restore result by copying csproj files separately
COPY *.sln .
COPY **/*.Infrastructure.csproj ./SearchService.Infrastructure/SearchService.Infrastructure.csproj
COPY **/*.Application.csproj ./SearchService.Application/SearchService.Application.csproj
COPY **/*.Interface.csproj ./SearchService.Interface/SearchService.Interface.csproj
RUN dotnet restore *.sln

# copies the rest of code
COPY . .
RUN dotnet publish --output /app/ --configuration Release

# Stage 2
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /app
COPY --from=builder /app .
ENTRYPOINT ["dotnet", "SearchService.Interface.dll"]