using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SearchService.Infrastructure.GoogleSearchService;
using SearchService.Infrastructure.Aws;
using SearchService.Application.Services;
using SearchService.Application.Parsers;
using SearchService.Application.Mappers;

namespace SearchService.Interface {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHealthChecks();

            services.AddCors(options => {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.ConfigureGoogleSearchService(Configuration);

            services
                .ConfigureAwsCredentials(Configuration)
                .ConfigureAwsComprehend();

            services
                .AddScoped<PublishDateParser, BasePublishDateParser>()
                .AddScoped<ArticlePublishDateParser, BaseArticlePublishDateParser>()
                .AddScoped<ArticleModifiedDateParser, BaseArticleModifiedDateParser>()
                .AddScoped<DescriptionParser, BaseDescriptionParser>()
                .AddScoped<CseImageParser, BaseCseImageParser>()
                .AddScoped<ResourceContractMapper>();

            services
                .AddScoped<ISearchService, GoogleSearchService>()
                .AddScoped<IKeyPhraseResolveService, AmazonKeyPhraseResolveService>()
                .AddScoped<EvaluationService, BaseEvaluationService>()
                .AddScoped<ResourcesService, BaseResourcesService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseHealthChecks(
                "/health",
                new HealthCheckOptions {
                    Predicate = check => check.Tags.Contains("ready")
                }
            );
        }
    }
}