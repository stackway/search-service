using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

using SearchService.Application.Contracts;
using System;

namespace SearchService.Interface.Contracts {
    public class CommonSearchQuery : BaseSearchQuery {
        [Required]
        [MinLength(1)]
        public override string BaseTerms { get; set; }
        public override string OptionalTerms { get; set; } = "";
        [Range(1, 10)]
        public override int Number { get; set; } = 3;
        [Range(1, 99)]
        public override int StartAt { get; set; } = 1;
        public override string DateRestrict { get; set; } = "m1";
        public override IEnumerable<string> QueryTags { get; set; }

        public override string ToString() {
            var queryString = "";

            if (BaseTerms != null) {
                queryString += $"\n\tbase: {BaseTerms}";
            }

            if (OptionalTerms != null) {
                queryString += $"\n\toptional: {OptionalTerms}";
            }

            queryString += $"\n\tcount: {Number}\n\tstart at: {StartAt}";

            if (DateRestrict != null) {
                queryString += $"\n\trestricted: {DateRestrict}";
            }

            if (QueryTags != null) {
                queryString += $"\n\t{String.Join("<|>", QueryTags)}";
            }

            return queryString;
        }
    }
}