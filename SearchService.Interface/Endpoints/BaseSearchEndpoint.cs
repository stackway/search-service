using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SearchService.Application.Contracts;
using SearchService.Application.Parsers;
using SearchService.Application.Services;
using SearchService.Interface.Contracts;

namespace SearchService.Interface.Endpoints {
    [Route("internal/api/resources/")]
    [EnableCors("CorsPolicy")]
    public class BaseSearchEndpoint : Controller {
        private readonly ResourcesService _service;
        private readonly ILogger _logger;

        public BaseSearchEndpoint(
            ResourcesService service,
            ILogger<BaseSearchEndpoint> logger
        ) {
            _service = service;
            _logger = logger;
        }

        [Route("common/")]
        [HttpPost]
        public async Task<IActionResult> SearchCommonAsync([FromBody] CommonSearchQuery query) {
            _logger.LogInformation($"[-x] query recivied: {query.ToString()}");

            return new JsonResult(
                await _service.FetchAsync(query)
            );
        }
    }
}
