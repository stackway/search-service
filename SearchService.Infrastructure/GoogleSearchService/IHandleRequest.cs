using static Google.Apis.Customsearch.v1.CseResource;

namespace SearchService.Infrastructure.GoogleSearchService {
    public interface IHandleRequest {
        ListRequest ApplyTo(ListRequest request);
    }
}