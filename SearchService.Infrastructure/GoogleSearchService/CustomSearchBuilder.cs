using System;

using Google.Apis.Customsearch.v1;
using static Google.Apis.Services.BaseClientService;

namespace SearchService.Infrastructure.GoogleSearchService {
    public class CustomSearchBuilder {
        private readonly Initializer _initializer;

        public CustomSearchBuilder(Initializer initializer) {
            _initializer = initializer;
        }

        public CustomSearchBuilder UseAuth(SearchAuth auth) {
            _initializer.ApiKey = auth.ApiKey;

            return this;
        }

        public CustomsearchService Build() {
            return new CustomsearchService(_initializer);
        }
    }
}