using System;

using static Google.Apis.Customsearch.v1.CseResource;

namespace SearchService.Infrastructure.GoogleSearchService {
    public class SearchAuth : IHandleRequest {
        public readonly string EngineId;
        public readonly string ApiKey;

        public SearchAuth(string engineId, string apiKey) {
            EngineId = engineId;
            ApiKey = apiKey;
        }

        public ListRequest ApplyTo(ListRequest request) {
            request.Cx = EngineId;

            return request;
        }
    }
}