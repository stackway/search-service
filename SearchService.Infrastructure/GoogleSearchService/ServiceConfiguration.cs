using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Google.Apis.Customsearch.v1;
using static Google.Apis.Services.BaseClientService;

namespace SearchService.Infrastructure.GoogleSearchService {
    public static class ServiceConfiguration {
        public static IServiceCollection ConfigureGoogleSearchService(
            this IServiceCollection services,
            IConfiguration config
        ) {
            var searchAuth = services.ConfigureGoogleSearchAuth(
                config
            );

            services.AddSingleton<CustomsearchService>(
                provider => new CustomSearchBuilder(new Initializer())
                    .UseAuth(searchAuth)
                    .Build()
            );

            return services;
        }

        public static SearchAuth ConfigureGoogleSearchAuth(
            this IServiceCollection services,
            IConfiguration config
        ) {
            var searchAuth = new SearchAuth(
                config["CustomGoogleSearch:EngineId"],
                config["CustomGoogleSearch:ApiKey"]
            );

            services.AddSingleton<SearchAuth>(searchAuth);

            return searchAuth;
        }
    }
}