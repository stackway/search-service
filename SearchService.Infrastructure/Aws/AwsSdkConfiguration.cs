using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

using Amazon.Runtime;
using Amazon.Comprehend;

namespace SearchService.Infrastructure.Aws {
    public static class AwsSdkConfiguration {
        public static IServiceCollection ConfigureAwsCredentials(
            this IServiceCollection services,
            IConfiguration config
        ) {
            var awsOptions = config.GetAWSOptions();
            awsOptions.Credentials = new EnvironmentVariablesAWSCredentials();

            return services.AddDefaultAWSOptions(awsOptions);
        }

        public static IServiceCollection ConfigureAwsComprehend(
            this IServiceCollection services
        ) {
            return services
                .AddAWSService<IAmazonComprehend>()
                .AddScoped<AmazonComprehendClient>();
        }
    }
}