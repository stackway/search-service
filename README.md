# Overview

## Google CustomSearch API

- <https://developers.google.com/custom-search/v1/cse/list>

## AWSSDK

- <https://stackoverflow.com/questions/43053495/how-to-set-credentials-on-aws-sdk-on-net-core>

## Test

```bash
# build
bash ./scripts/local-build-docker-image.bash

# run
docker run --name search-service-test --rm -p 8000:80 registry.gitlab.com/stackway/search-service
```
