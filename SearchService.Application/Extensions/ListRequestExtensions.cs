using System.Collections.Generic;

using static Google.Apis.Customsearch.v1.CseResource;

using SearchService.Infrastructure.GoogleSearchService;

namespace SearchService.Application.Extensions {
    public static class ListRequestExtensions {
        public static ListRequest Use(
            this ListRequest context,
            params IHandleRequest[] handlers
        ) {
            foreach (var handler in handlers)
                handler.ApplyTo(context);

            return context;
        }
    }
}