using System.Collections.Generic;

namespace SearchService.Application.Parsers {
    public interface PageMapParser<T> {
        T Parse(IDictionary<string, IList<IDictionary<string, object>>> pageMap);
    }
}