using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchService.Application.Parsers {
    public interface PublishDateParser : PageMapParser<DateTime?> { }

    public class BasePublishDateParser : PublishDateParser {
        public DateTime? Parse(IDictionary<string, IList<IDictionary<string, object>>> pageMap) {
            if (!pageMap.ContainsKey("metatags")) {
                return new DateTime?();
            }

            var metatags = pageMap["metatags"];
            if (!metatags.Any() || !metatags.First().ContainsKey("article:published_time")) {
                return new DateTime?();
            }

            try {
                var publishedAt = metatags.First()["article:published_time"];

                if (publishedAt is DateTime dateTime) {
                    return dateTime;
                }

                return DateTime.Parse(publishedAt as string);
            }
            catch (ArgumentNullException) {
                return new DateTime?();
            }
            catch (FormatException) {
                return new DateTime?();
            }
        }
    }
}