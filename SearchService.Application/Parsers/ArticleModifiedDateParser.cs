using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchService.Application.Parsers {
    public interface ArticleModifiedDateParser : PageMapParser<DateTime?> { }

    public class BaseArticleModifiedDateParser : ArticleModifiedDateParser {
        public DateTime? Parse(IDictionary<string, IList<IDictionary<string, object>>> pageMap) {
            if (!pageMap.ContainsKey("article")) {
                return new DateTime?();
            }

            var article = pageMap["article"];
            if (!article.Any() || !article.First().ContainsKey("datemodified")) {
                return new DateTime?();
            }
            
            try {
                var publishedAt = article.First()["datemodified"];

                if (publishedAt is DateTime dateTime) {
                    return dateTime;
                }

                return DateTime.Parse(publishedAt as string);
            }
            catch (ArgumentNullException) {
                return new DateTime?();
            }
            catch (FormatException) {
                return new DateTime?();
            }
        }
    }
}