using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchService.Application.Parsers {
    public interface ArticlePublishDateParser : PageMapParser<DateTime?> { }

    public class BaseArticlePublishDateParser : ArticlePublishDateParser {
        public DateTime? Parse(IDictionary<string, IList<IDictionary<string, object>>> pageMap) {
            if (!pageMap.ContainsKey("article")) {
                return new DateTime?();
            }

            var article = pageMap["article"];
            if (!article.Any() || !article.First().ContainsKey("datepublished")) {
                return new DateTime?();
            }
            
            try {
                var publishedAt = article.First()["datepublished"];

                if (publishedAt is DateTime dateTime) {
                    return dateTime;
                }

                return DateTime.Parse(publishedAt as string);
            }
            catch (ArgumentNullException) {
                return new DateTime?();
            }
            catch (FormatException) {
                return new DateTime?();
            }
        }
    }
}