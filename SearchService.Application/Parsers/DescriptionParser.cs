using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchService.Application.Parsers {
    public interface DescriptionParser : PageMapParser<string> { }

    public class BaseDescriptionParser : DescriptionParser {
        public string Parse(IDictionary<string, IList<IDictionary<string, object>>> pageMap) {
            if (!pageMap.ContainsKey("metatags")) {
                return String.Empty;
            }

            var metatags = pageMap["metatags"];
            if (!metatags.Any() || !metatags.First().ContainsKey("og:description")) {
                return String.Empty;
            }

            try {
                return metatags.First()["og:description"] as string;
            }
            catch (InvalidCastException) {
                return String.Empty;
            }
        }
    }
}