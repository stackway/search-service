using System;
using System.Collections.Generic;
using System.Linq;

namespace SearchService.Application.Parsers {
    public interface CseImageParser : PageMapParser<string> { }

    public class BaseCseImageParser : CseImageParser {
        public string Parse(IDictionary<string, IList<IDictionary<string, object>>> pageMap) {
            if (!pageMap.ContainsKey("cse_image")) {
                return String.Empty;
            }

            var image = pageMap["cse_image"];
            if (!image.Any() || !image.First().ContainsKey("src")) {
                return String.Empty;
            }

            try {
                return image.First()["src"] as string;
            }
            catch (InvalidCastException) {
                return String.Empty;
            }
        }
    }
}