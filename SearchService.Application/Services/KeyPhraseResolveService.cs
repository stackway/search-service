using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Amazon.Comprehend;
using Amazon.Comprehend.Model;

namespace SearchService.Application.Services {
    public interface IKeyPhraseResolveService {
        Task<IEnumerable<string>> ResolveKeyPhrasesAsync(
            string analyzable, string language
        );
        Task<IEnumerable<IEnumerable<string>>> ResolveKeyPhrasesBatchAsync(
            IEnumerable<string> analyzable, string language
        );
    }

    public class AmazonKeyPhraseResolveService : IKeyPhraseResolveService {
        private readonly AmazonComprehendClient _client;
        private readonly ILogger _logger;

        public AmazonKeyPhraseResolveService(
            AmazonComprehendClient client,
            ILogger<AmazonKeyPhraseResolveService> logger
        ) {
            _client = client;
            _logger = logger;
        }

        /// <param name="analyzable">text content to analyze</param>
        /// <param name="language">is a language code, i.e. 'en' or 'de'</param>
        public async Task<IEnumerable<string>> ResolveKeyPhrasesAsync(
            string analyzable, string language
        ) {
            var request = new DetectKeyPhrasesRequest() {
                Text = analyzable,
                LanguageCode = language
            };

            try {
                var keyPhrases = await _client.DetectKeyPhrasesAsync(request);

                return keyPhrases.KeyPhrases.Select(phrase => phrase.Text);
            }
            catch (IOException exception) {
                _logger.LogError(exception.Message);

                return new List<string>();
            }
        }

        public async Task<IEnumerable<IEnumerable<string>>> ResolveKeyPhrasesBatchAsync(
            IEnumerable<string> analyzable, string language
        ) {
            var request = new BatchDetectKeyPhrasesRequest() {
                TextList = analyzable.ToList(),
                LanguageCode = language
            };

            try {
                var keyPhrases = await _client.BatchDetectKeyPhrasesAsync(request);

                return keyPhrases.ResultList
                    .Select(tags => tags
                        .KeyPhrases
                        .Select(phrase => phrase.Text)
                    );
            }
            catch (IOException exception) {
                _logger.LogError(exception.Message);

                return new List<List<string>>();
            }

        }
    }
}