using System.Collections.Generic;
using System.Linq;

namespace SearchService.Application.Services {
    public interface EvaluationService {
        double Evaluate(IEnumerable<string> queryTags, IEnumerable<string> actualTags);
    }

    public class BaseEvaluationService : EvaluationService {
        public double Evaluate(IEnumerable<string> queryTags, IEnumerable<string> actualTags) {
            var performedActualTags = actualTags.Select(t => t.ToLower());

            var splicedQueryTags = queryTags
                .Select(t => t.ToLower().Split(' '))
                .Aggregate(
                    new List<string>(),
                    (acc, splicedTags) => acc.Concat(splicedTags).ToList()
                );

            var foundSplicedTags = splicedQueryTags.Count(
                t => performedActualTags.Any(a => a.Contains(t))
            );

            return (double) foundSplicedTags / splicedQueryTags.Count;
        }
    }
}