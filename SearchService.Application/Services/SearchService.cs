using System;
using System.Threading.Tasks;

using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;

using SearchService.Infrastructure.GoogleSearchService;
using SearchService.Application.Extensions;
using SearchService.Application.Contracts;

namespace SearchService.Application.Services {
    public interface ISearchService {
        Task<Search> Fetch(BaseSearchQuery query);
    }

    public class GoogleSearchService : ISearchService {
        private readonly CustomsearchService _searchEngine;
        private readonly SearchAuth _auth;

        public GoogleSearchService(CustomsearchService searchEngine, SearchAuth auth) {
            _searchEngine = searchEngine;
            _auth = auth;
        }

        public Task<Search> Fetch(BaseSearchQuery query) {
            return _searchEngine.Cse
                .List(query.BaseTerms)
                .Use(query, _auth)
                .ExecuteAsync();
        }
    }
}