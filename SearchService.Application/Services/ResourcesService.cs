using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SearchService.Application.Contracts;
using SearchService.Application.Mappers;
using SearchService.Application.Parsers;

namespace SearchService.Application.Services {
    public interface ResourcesService {
        Task<IEnumerable<ResourceContract>> FetchAsync(BaseSearchQuery query);
    }

    public class BaseResourcesService : ResourcesService {
        private readonly ISearchService _searchService;
        private readonly IKeyPhraseResolveService _keyPhraseResolver;
        private readonly EvaluationService _evaluationService;
        private readonly DescriptionParser _descriptionParser;
        private readonly ResourceContractMapper _mapper;
        private readonly ILogger _logger;

        public BaseResourcesService(
            ISearchService searchService,
            IKeyPhraseResolveService keyPhraseResolve,
            EvaluationService evaluationService,
            DescriptionParser descriptionParser,
            ResourceContractMapper mapper,
            ILogger<BaseResourcesService> logger
        ) {
            _searchService = searchService;
            _keyPhraseResolver = keyPhraseResolve;
            _evaluationService = evaluationService;
            _descriptionParser = descriptionParser;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IEnumerable<ResourceContract>> FetchAsync(BaseSearchQuery query) {
            var fetchedArticles = await _searchService.Fetch(query);

            _logger.LogInformation(JsonConvert.SerializeObject(fetchedArticles));

            var articles = fetchedArticles.Items;

            if (articles == null) {
                _logger.LogWarning("can't find any articles");

                return new List<ResourceContract>();
            }

            var keyPhrases = await _keyPhraseResolver.ResolveKeyPhrasesBatchAsync(
                articles.Select(article => {
                    var description = _descriptionParser.Parse(article.Pagemap);

                    if (String.IsNullOrEmpty(description)) {
                        description = article.Snippet;
                    }

                    return $"{article.Title} . {description}";
                }),
                "en"
            );

            var resources = new List<ResourceContract>();

            for (int i = 0; i < articles.Count; i++) {
                IEnumerable<string> tags = new List<string>();

                if (keyPhrases.Count() > i) {
                    tags = keyPhrases.ElementAt(i);
                }

                resources.Add(_mapper
                    .Map(articles[i], query, tags)
                    .UpdateRelevanceIndex(_evaluationService)
                );
            }

            return resources;
        }
    }
}