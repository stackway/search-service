using System;
using System.Collections.Generic;
using System.Linq;
using Google.Apis.Customsearch.v1.Data;
using SearchService.Application.Contracts;
using SearchService.Application.Parsers;

namespace SearchService.Application.Mappers {
    public class ResourceContractMapper {
        private readonly PublishDateParser _publishDateParser;
        private readonly ArticlePublishDateParser _articlePublishDateParser;
        private readonly ArticleModifiedDateParser _modifiedDateParser;
        private readonly CseImageParser _cseImageParser;

        public ResourceContractMapper(
            PublishDateParser publishDateParser,
            ArticlePublishDateParser articlePublishDateParser,
            ArticleModifiedDateParser modifiedDateParser,
            CseImageParser cseImageParser
        ) {
            _publishDateParser = publishDateParser;
            _articlePublishDateParser = articlePublishDateParser;
            _modifiedDateParser = modifiedDateParser;
            _cseImageParser = cseImageParser;
        }

        public ResourceContract Map(
            Result item,
            BaseSearchQuery query,
            IEnumerable<string> tags
        ) {
            if (!tags.Any() && item.Labels != null) {
                tags = item.Labels
                    .Select(label => label.Name)
                    .ToList();
            }

            DateTime? publishedTime = _publishDateParser.Parse(item.Pagemap);

            if (!publishedTime.HasValue) {
                publishedTime = _articlePublishDateParser.Parse(item.Pagemap);
            }

            if (!publishedTime.HasValue) {
                publishedTime = _modifiedDateParser.Parse(item.Pagemap);
            }

            string imageLink = _cseImageParser.Parse(item.Pagemap);

            return new ResourceContract {
                Title = item.Title,
                Description = item.Snippet,

                DisplayLink = item.DisplayLink,
                Link = item.Link,

                ImageLink = imageLink,

                PublishedAt = publishedTime,
                Tags = tags,

                QueryTags = query.QueryTags
            };
        }
    }
}