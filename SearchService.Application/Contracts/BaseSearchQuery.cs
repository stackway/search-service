using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static Google.Apis.Customsearch.v1.CseResource;
using static Google.Apis.Customsearch.v1.CseResource.ListRequest;
using SearchService.Infrastructure.GoogleSearchService;

namespace SearchService.Application.Contracts {
    public abstract class BaseSearchQuery : ISearchQuery, IHandleRequest {
        public abstract string BaseTerms { get; set; }
        public abstract string OptionalTerms { get; set; }

        public abstract int Number { get; set; }

        public abstract int StartAt { get; set; }
        public abstract string DateRestrict { get; set; }

        public abstract IEnumerable<string> QueryTags { get; set; }

        public BaseSearchQuery UseQueryTags(IEnumerable<string> tags) {
            QueryTags = tags;

            return this;
        }

        /// Attention!
        /// The CustomSearchService is aggressive API
        /// and can throws many different exceptions
        /// with zero debug info, so be patient
        /// https://stackoverflow.com/questions/30956186/messageinvalid-value-location-reasoninvalid-domainglobal
        public ListRequest ApplyTo(ListRequest request) {
            request.DateRestrict = DateRestrict;

            // CustomSearchService doesn't provide
            // more than 100 results 
            if (StartAt > 2 && StartAt <= 90)
                request.Start = StartAt;

            if (Number > 0 && Number <= 10)
                request.Num = Number;

            request.OrTerms = OptionalTerms;

            request.Lr = LrEnum.LangEn;

            return request;
        }
    }
}