using System;
using System.Collections.Generic;
using SearchService.Application.Services;

namespace SearchService.Application.Contracts {
    public class ResourceContract {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? PublishedAt { get; set; }

        public string DisplayLink { get; set; }
        public string Link { get; set; }
        public string ImageLink { get; set; }

        public double RelevanceIndex { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public IEnumerable<string> QueryTags { get; set; }

        public ResourceContract UpdateRelevanceIndex(EvaluationService service) {
            RelevanceIndex = service.Evaluate(QueryTags, Tags);
            
            return this;
        }
    }
}