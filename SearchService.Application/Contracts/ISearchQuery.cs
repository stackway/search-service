using System.Collections.Generic;

namespace SearchService.Application.Contracts {
    public interface ISearchQuery {
        string BaseTerms { get; }
        string OptionalTerms { get; }

        int Number { get; }

        int StartAt { get; }
        string DateRestrict { get; }

        IEnumerable<string> QueryTags { get; }
    }
}