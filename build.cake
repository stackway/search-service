#addin "Cake.Docker"

var target = Argument("target", "Default");
var solution = Argument("solution", "SearchService.sln");
var configuration = Argument("configuration", "Release");

var image = Argument("image", "");

var registry = Argument("registry", "");
var username = Argument("user", "");
var password =  Argument("pass", "");

Task("Restore").Does(() => {
   DotNetCoreRestore();
});

Task("Clean").Does(() => {
   DotNetCoreClean(solution);
});

Task("Build")
   .IsDependentOn("Restore")
   .IsDependentOn("Clean")
   .Does(() => {
      var settings = new DotNetCoreBuildSettings {
         Configuration = configuration
      };

      DotNetCoreBuild(solution, settings);
   });

Task("DockerSetup").Does(() => {
   DockerLogin(username, password, registry);
});

Task("SetupImageTag").Does(() => {
   if (String.IsNullOrEmpty(image)) {
      image = $"{registry}/stackway/search-service";
   }
});

Task("BuildImage")
   .IsDependentOn("SetupImageTag")
   .Does(() => {
      var settings = new DockerImageBuildSettings() {
         Tag = new string[] {image}
      };

      DockerBuild(settings, ".");
   });

Task("PushImage")
   .IsDependentOn("DockerSetup")
   .IsDependentOn("BuildImage")
   .Does(() => {
      DockerPush(image);
   });

Task("Default").IsDependentOn("Build");

RunTarget(target);
